## Simulation

Pour lancer la simulation :

```shell
    python3 main.py
```

Afin de modifier les paramètres, il faut juste éditer le 'main.py'

```python
    #Choix d'un grand nombre qui correspond au nombre d'itération
    grdNombre = 100#int(1E15)
    #Appel d'une simulation avec parametres
    Nc, Nt, Ntmax = 1000, 99, 1099
```

## Analyse des résultats

Pour lancer le merge des résultats :

```shell
    python3 merge.py
```

La sortie du merge est un fichier csv 'resultat_merge.csv', il sera dértuit et recrée à chaque lancement du 'merge.py'