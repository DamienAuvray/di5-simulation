from echeancier import Echeancier
from indicateur import Indicateur
import random

#Simulation
class Simulation:

    #Initialise le simulateur
    def __init__(self, Nc=8, Nt=2, Ntmax=4):
        self.Nc = Nc
        self.Nt = Nt
        self.Ntmax = Ntmax
        self.AireQc = 0
        self.AireQt = 0
        self.AireBc = 0
        self.AireBt = 0
        self.Simulateur()

    #Debut Simulateur
    def Simulateur(self):
        #On initialise l'échéancier
        self.echeancier = Echeancier()
        self.Hs = 0
        self.HsPrecedent = 0
        #Créer et insère l’évènement “Début” dans l'échéancier à l’instant 8*60
        self.echeancier.Ajouter("Debut", 8*60)

        #(E, H ) <- 1er événement de l’échéancier
        premierEvenement = self.echeancier.Executer() 
        # Hs <- H
        self.Hs = premierEvenement[1] 
        #Exécuter (E, H)
        getattr(self, premierEvenement[0])()
        self.HsPrecedent = premierEvenement[1]

        #Faire
        while True:
            #(E, H ) <- 1er événement de l’échéancier
            premierEvenement = self.echeancier.Executer()   
            # Hs <- H
            self.Hs = premierEvenement[1]         
            #Affichage des étapes
            
            #mise a jour des aires de Qc, Qt, Bc et Bt
            self.AireQc = self.AireQc + (premierEvenement[1] - self.HsPrecedent) * self.Qc
            self.AireQt = self.AireQt + (premierEvenement[1] - self.HsPrecedent) * self.Qt
            self.AireBc = self.AireBc + (premierEvenement[1] - self.HsPrecedent) * self.Bc
            self.AireBt = self.AireBt + (premierEvenement[1] - self.HsPrecedent) * self.Bt
            
            '''if (self.AireQt != 0):
                print(str(round(self.Hs,2)) + " | " + premierEvenement[0])# + " | " + str(self.echeancier.echeancier))
                print(self.AireQt,self.Qt,(premierEvenement[1] - self.HsPrecedent))
                input("Press Enter to continue...")'''
            #print(premierEvenement[0]+", diff="+str(premierEvenement[1] - self.HsPrecedent))

            #Exécuter (E, H)
            getattr(self, premierEvenement[0])()

            self.HsPrecedent = premierEvenement[1]

            #if(self.Bt>self.Nt or self.Bc>self.Nc or (self.Bc+self.Bt)>self.N or (self.Nc+self.Nt)!=self.N):
            #    print('\n'.join("%s: %s" % item for item in vars(self).items()))
            #Tant que l’échéancier est non vide
            if self.echeancier.EcheancierVide():
                break
    #Fin Simulateur

    #Debut Debut
    def Debut(self):
        #Initialisation des variables
        self.N = self.Nt + self.Nc
        #Initialisation des queues
        self.Qc = int(random.uniform(20, 80))
        self.NBc = self.Qc
        self.NBt = 0
        self.Qt = 0
        #Toujours à 0 au début
        self.Bc = 0
        self.Bt = 0
        self.Hs = 480
        #Créer et insère l’évènement "ArrC" dans l'échéancier à l’instant Hs + E(1/5)
        self.echeancier.Ajouter("ArrC", self.Hs+ random.expovariate(2))
        #Créer et insère l’évènement "ArrC" dans l'échéancier à l’instant Hs + E(2)
        self.echeancier.Ajouter("ArrT", self.Hs+random.expovariate(1/5))
        #Créer et insère l’évènement "Fin" dans l'échéancier à l’instant self.Hs+14*60
        self.echeancier.Ajouter("Fin", 12*60)
    #Fin Debut

    #Debut Fin
    def Fin(self):
        #Calcul des résultats
        self.indicateur = Indicateur(self)
        #On vide l'échéancier
        self.echeancier.Vider()
    #Fin Fin

    #ArrC
    def ArrC(self):
        #Qc <- Qc + 1
        self.Qc = self.Qc + 1
        #NBc <- NBc + 1
        self.NBc = self.NBc + 1
        #Si 8 <= Hs < 9
        if 8*60 <= self.Hs and 9*60 > self.Hs:
            #avg <- 0.5
            avg = 0.5
        #Sinon
        else:
            #avg <- 5
            avg = 5
        #Créer et insère l’évènement "ArrC" dans l'échéancier à l’instant Hs + E(avg)
        self.echeancier.Ajouter("ArrC", self.Hs+ random.expovariate(1/avg))
        #Si Bc < Nc
        if self.Bc < self.Nc:
        #Créer et insère l’évènement "DebRc" dans l'échéancier à l’instant Hs
            self.echeancier.Ajouter("DebRc", self.Hs)
    #Fin ArrC

    #Debut ArrT
    def ArrT(self):
        #Qt <- Qt + 1
        self.Qt = self.Qt + 1
        #NBt <- NBt + 1
        self.NBt = self.NBt + 1
        #Si 8 <= Hs < 9
        if 8*60 <= self.Hs and 9*60 > self.Hs:
            #avg <- 5
            avg = 5
        #Si 9 <= Hs < 11
        elif 9*60 <= self.Hs and 11*60 > self.Hs:
            #avg <- 1
            avg = 1
        #Sinon
        else:
            #avg <- 10
            avg = 10
        #Créer et insère l’évènement "ArrT" dans l'échéancier à l’instant Hs + E(avg)
        self.echeancier.Ajouter("ArrT", self.Hs + random.expovariate(1/avg))
        #Si Bt < Nt
        if self.Bt < self.Nt:
            #Créer et insère l’évènement "DebRt" dans l'échéancier à l’instant Hs
            self.echeancier.Ajouter("DebRt", self.Hs)
    #Fin ArrT

    #Debut DebRc
    def DebRc(self):
        #Qc <- Qc - 1
        self.Qc = self.Qc - 1
        #Bc <- Bc - 1
        self.Bc = self.Bc + 1
        #Créer et insère l’évènement "FinRc" dans l'échéancier à l’instant Hs + U(3,7)
        self.echeancier.Ajouter("FinRc", self.Hs + random.uniform(3, 7))
    #Fin DebRc

    #Debut DebRt
    def DebRt(self):
        #Qt <- Qt - 1
        self.Qt = self.Qt - 1
        #Bt <- Bt - 1
        self.Bt = self.Bt + 1
        #Créer et insère l’évènement "FinRt" dans l'échéancier à l’instant Hs + U(5,15)
        self.echeancier.Ajouter("FinRt", self.Hs + random.uniform(5, 15))
    #Fin DebRt

    #Debut FinRc
    def FinRc(self):
        #Bc <- Bc - 1
        self.Bc = self.Bc - 1
        #Créer et insère l’évènement "ReorgNc" dans l'échéancier à l’instant Hs
        self.echeancier.Ajouter("ReorgNc", self.Hs)
    #Fin FinRc

    #Debut FinRt
    def FinRt(self):
        #Bt <- Bt - 1
        self.Bt = self.Bt - 1
        #Créer et insère l’évènement "ReorgNc" dans l'échéancier à l’instant Hs
        self.echeancier.Ajouter("ReorgNt", self.Hs)
    #Fin FinRc

    #Debut ReorgNc
    def ReorgNc(self):
        #Si Qt >= Nt et Nt < Ntmax
        if self.Qt >= self.Nt and self.Nt < self.Ntmax:
            #Nt <- Nt + 1
            self.Nt = self.Nt + 1
            #Nc <- Nc - 1
            self.Nc = self.Nc - 1
            if self.Qt > 0:
                #Créer et insère l’évènement "DebRt" dans l'échéancier à l’instant Hs
                self.echeancier.Ajouter("DebRt", self.Hs)
        else:            
            if self.Qc > 0:
                #Créer et insère l’évènement "DebRc" dans l'échéancier à l’instant Hs
                self.echeancier.Ajouter("DebRc", self.Hs)
    #Fin ReorgNc

    #Debut ReorgNt
    def ReorgNt(self):
        #Si Qt = 0
        if self.Qt == 0:
            #Nt <- Nt - 1
            self.Nt = self.Nt - 1
            #Nc <- Nc + 1
            self.Nc = self.Nc + 1
            if self.Qc > 0:
                #Créer et insère l’évènement "DebRc" dans l'échéancier à l’instant Hs
                self.echeancier.Ajouter("DebRc", self.Hs)
        #Sinon
        else:
            if self.Qt > 0:
                #Créer et insère l’évènement "DebRt" dans l'échéancier à l’instant Hs
                self.echeancier.Ajouter("DebRt", self.Hs)
    #Fin ReorgNt
