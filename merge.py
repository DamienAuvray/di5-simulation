from collections import defaultdict
from statistics import mean
import csv

#Merge les lignes du tableau passé en paramêtre sous forme de chaine de caractere
def merge(rows):
    lst = []
    #Pour chaque ligne du tableau on les converties en array de float
    for row in rows:
        #On split les virgues
        results = row.split(",")
        #On ajoute le resultat dans un nouveau tableau
        lst.append(list(map(float, results)))
    #On calcule la moyenne et on retourne le résultat
    return list(map(mean, zip(*lst)))

#On créer un dictionnaire de liste
result=defaultdict(list)
#On prepare un tableau de résultat
rows = []
#Pour chaque ligne du csv
with open('resultat.csv') as csvfile:
    #On extrait les lignes
    reader = csv.reader(csvfile, delimiter=',')
    #On sauvegarde la première ligne qui correspond au header
    rows.append(next(reader, None))
    #Pour chacunes des lignes du contenu
    for row in reader:
        #Si elle existe
        if any(row):
            idx = ""
            #On les tries par paramêtres (Création des clés)
            for i in range(0,3):
                idx = idx+str(row[i])+","
            values = ""
            #On sauvegare le contenu sous forme de chaine de caractère
            for i in range(3,len(row)):
                values = values+str(row[i])+","
            #On ajoute les contenu en fonction de la clé
            result[idx[:-1]].append(values[:-1])
    #Pour chaque élément du dictionnaire
    for i in result.keys():
        #On calcul la moyenne et on ajoute le résultat à la listes
        rows.append(list(map(float, i.split(",")))+merge(result.get(i)))
#On créer un nouveau fichier pour sauvegarder les résultats
f = open('resultat_merge.csv', "w+")
#Fermeture du fichier
f.close()
#On ouvre le fichier pour sauvegarder les résultats
with open('resultat_merge.csv','a') as fd:
    #On prepare le fichier au format CSV
    writer = csv.writer(fd, delimiter=',')
    #Pour chaque ligne de nos résultats
    for line in rows:
        #On écrit le contenu en tant que nouvelle ligne
        writer.writerow(line)