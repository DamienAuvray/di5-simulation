import random
import numpy
from scipy.stats import ks_2samp

serieTh = []
serieExp = []

serieExp.append(17.76)
serieExp.append(16.04)
serieExp.append(12.9300000000001)
serieExp.append(10.7900000000001)
serieExp.append(10.54)
serieExp.append(9.99000000000001)
serieExp.append(9.5)
serieExp.append(8.63)
serieExp.append(7.78999999999996)
serieExp.append(7.11000000000001)
serieExp.append(6.73000000000002)
serieExp.append(6.64999999999998)
serieExp.append(5.14999999999998)
serieExp.append(4.19000000000005)
serieExp.append(3.93999999999994)
serieExp.append(3.75999999999999)
serieExp.append(3.51999999999998)
serieExp.append(3.00999999999999)
serieExp.append(2.83000000000004)
serieExp.append(2.69000000000005)
serieExp.append(2.53000000000009)
serieExp.append(2.29999999999995)
serieExp.append(1.61000000000001)
serieExp.append(1.25)
serieExp.append(1.09000000000003)
serieExp.append(0.919999999999959)
serieExp.append(0.860000000000014)
serieExp.append(0.809999999999945)
serieExp.append(0.74)
serieExp.append(0.589999999999918)
serieExp.append(0.42999999999995)
serieExp.append(0.340000000000032)
serieExp.append(0.00999999999999091)

pvalueResult = []

for nbite in range(0,50):
    serieTh.clear()
    
    for i in range(0,33):
        serieTh.append(random.expovariate(1/5))

    serieTh.sort()
    serieTh.reverse()

    pvalueResult.append(ks_2samp(serieTh,serieExp).pvalue)

print(numpy.mean(pvalueResult))