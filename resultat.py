from simulation import Simulation
from indicateur import Indicateur

#Classe permettant d'écrire les résultats
class Resultat:

    def __init__(self, Nc, Nt, Ntmax, grdNombre):
        #Définition des parametres
        self.Nc = Nc
        self.Nt = Nt
        self.Ntmax = Ntmax
        self.grdNombre = grdNombre
        #Definition des indicateurs statistiques
        self.sommeNBcNT = 0
        self.sommeTAt = 0
        self.sommeCRc = 0
        self.sommeNBc = 0
        self.sommeNBt = 0
        self.sommeAireB = 0
        self.sommeTOE = 0

    #Calcul de la moyenne pour chaque élément
    def calculMoyenne(self, simulation, NBiteration):
        #Calcul moyenne NBcNT
        self.sommeNBcNT = self.sommeNBcNT + simulation.indicateur.NBcNT
        self.moyenneNBcNT = self.sommeNBcNT/NBiteration
        #Calcul moyenne TAt
        self.sommeTAt = self.sommeTAt + simulation.indicateur.TAt
        self.moyenneTAt = self.sommeTAt/NBiteration
        #Calcul moyenne CRc
        self.sommeCRc = self.sommeCRc + simulation.indicateur.CRc
        self.moyenneCRc = self.sommeCRc/NBiteration
        #Calcul moyenne NBc
        self.sommeNBc = self.sommeNBc + simulation.indicateur.NBc
        self.moyenneNBc = self.sommeNBc/NBiteration
        #Calcul moyenne aireB
        self.sommeAireB = self.sommeAireB + simulation.indicateur.AireB
        self.moyenneAireB = self.sommeAireB/NBiteration
        #Calcul moyenne TOE
        self.sommeTOE = self.sommeTOE + simulation.indicateur.TOE
        self.moyenneTOE = self.sommeTOE/NBiteration
        #Calcul moyenne NBt
        self.sommeNBt = self.sommeNBt + simulation.indicateur.NBt
        self.moyenneNBt = self.sommeNBt/NBiteration

    #Converti l'objet en liste
    def convertToArray(self):
    	return [self.Nc, self.Nt, self.Ntmax, self.grdNombre, self.moyenneNBcNT, self.moyenneTAt, self.moyenneCRc, self.moyenneNBc, self.moyenneNBt, self.moyenneTOE]