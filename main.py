from simulation import Simulation
from resultat import Resultat
import multiprocessing
import datetime
import csv 
import sys

'''#Main class
class MainApplication:
    #Appel d'une simulation sans parametres
    #Simulation()

    #Choix d'un grand nombre qui correspond au nombre d'itération
    grdNombre = 1000#int(1E15)
    #Appel d'une simulation avec parametres
    Nc, Nt, Ntmax = 8, 2, 10
    Simulation(Nc, Nt, Ntmax)
    #On prepare la méthode de calcul des moyennes
    start_time = datetime.datetime.now()
    resultat = Resultat(Nc, Nt, Ntmax, grdNombre)
    for x in range(1, grdNombre):
        simulation = Simulation(Nc, Nt, Ntmax)
        resultat.calculMoyenne(simulation, x)
    print(resultat.convertToArray())
    stop_time = datetime.datetime.now()
    print("Duration: ", stop_time - start_time)
'''

#Main class
def run(self):
    #Choix d'un grand nombre qui correspond au nombre d'itération
    grdNombre = int(250)#int(1E15)
    #Appel d'une simulation avec parametres
    Nc, Nt, Ntmax = int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3])
    #Nc, Nt, Ntmax = 8, 2, 10
    #On prepare la méthode de calcul des moyennes
    resultat = Resultat(Nc, Nt, Ntmax, grdNombre*multiprocessing.cpu_count())
    for x in range(1, grdNombre):
        simulation = Simulation(Nc, Nt, Ntmax)
        resultat.calculMoyenne(simulation, x)
    return resultat

#Calcul de la moyenne depuis la liste
def average(nums, default=float('nan')):
    return sum(nums) / float(len(nums)) if nums else default

#Windows needed
if __name__ == '__main__':
    #Debut du programme
    start_time = datetime.datetime.now()
    #Preparation de la Pool en fonction du nombre de cpu
    p = multiprocessing.Pool(processes=multiprocessing.cpu_count())
    #Execution de la simulation en fonction du nombre de core
    result_list = p.map(run, range(multiprocessing.cpu_count()))
    p.close()
    p.join()
    #Préparation de la liste pour la sortie
    result = []
    #Pour chaque élément à la fin de l'exécution
    for r in result_list:
        #On converti l'objet result en liste
        result.append(r.convertToArray())
    #Pour chaque élément de la liste on calcule la moyenne des valeurs 1 à 1
    average = [average(n) for n in zip(*result)]
    #On écrit le résultat dans un fichier CSV
    with open('resultat.csv','a') as fd:
        writer = csv.writer(fd)
        writer.writerow(average)
    #Fin du programme
    stop_time = datetime.datetime.now()
    #Afficheage du temps d'exécution
    print("Duration: ", stop_time - start_time)
