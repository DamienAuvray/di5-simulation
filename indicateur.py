class Indicateur:

    #################################
    ### Indicateurs statistiques ###
    # NBcNT = nb courriel non traités (= Qc à la fin)
    # TAt = temps d'attente moyen au téléphone
    # CRc = délais de réponse moyen courriel (= 1/NBc*AireQc)
    # NBc = nb courriel 
    # TOE = taux d'occupation employé 
    # AireB = AireBc + AireBt
    # AireQc, Qt, Bc, Bt
    #################################
    def __init__(self, simulation):
        self.NBcNT = simulation.Qc
        self.TAt = (1/simulation.NBt)*simulation.AireQt
        self.CRc = (1/simulation.NBc)*simulation.AireQc
        self.NBc = simulation.NBc
        self.NBt = simulation.NBt
        self.AireB = simulation.AireBc + simulation.AireBt
        self.TOE = self.AireB * 1/(simulation.N*(simulation.Hs-8*60))
        #print('\n'.join("%s: %s" % item for item in vars(self).items()))