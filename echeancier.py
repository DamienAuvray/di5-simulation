#Echéancier de la simulation
class Echeancier:

    #Initialise l'échéancier
    def __init__(self):
        self.echeancier = []

    #Récupère le dernier élément de la liste
    def Executer(self):
        #Récupère le premier élément de l'échéancier
        executer = self.echeancier[0]
        #Supprime l'élément à exécuter
        del self.echeancier[0]
        #retourne la valeur de l'élément à exécuter
        return executer

    #Ajoute un évenement à l'instant t
    def Ajouter(self, evenement, instant):
        #Ajoute l'élément à l'écheancier
        self.echeancier.append([evenement, instant])
        #Trie l'échéancier par chonologie
        self.echeancier.sort(key=lambda x: x[1])

    #Vérifie que l'échéancier est vide
    def EcheancierVide(self):
        #Si vide alors Vrai
        if len(self.echeancier) == 0:
            return True
        #Sinon Faux
        else:
            return False

    #Vide l'échéancier
    def Vider(self):
        self.echeancier = []